import React from 'react';
import { Grid } from '@mui/material';
import TeamMemberCard from '@/04_organisms/TeamMemberCard';
import useCharacters from '@/00_global/hooks/useCharacters';
import SectionTitle from '@/02_atoms/SectionTitle';

const TeamSelectionAside = () => {
	const { characters } = useCharacters();

	return (
		<>
			<SectionTitle>My Team</SectionTitle>
			<Grid container spacing={2}>
				{characters
					?.filter((character) => character.selected)
					.map((character) => (
						<Grid key={character.id} item xs>
							<TeamMemberCard character={character} />
						</Grid>
					))}
			</Grid>
		</>
	);
};

export default TeamSelectionAside;
