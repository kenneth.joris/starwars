import '@testing-library/jest-dom';
import { render } from '@testing-library/react';
import TeamSelectionAside from './index';
import { Provider } from 'react-redux';
import store from '../../00_global/store';
import { mockedCharacter } from '@/00_global/__mocks__';

let characterMock = mockedCharacter;
jest.mock('../../00_global/hooks/useCharacters', () => {
	return jest.fn(() => ({
		characters: [characterMock]
	}));
});

describe('TeamSelectionAside', () => {
	it('shows the character if it is selected', () => {
		const { getByText } = render(
			<Provider store={store}>
				<TeamSelectionAside />
			</Provider>
		);
		expect(getByText('Luke Skywalker')).toBeTruthy();
	});
	it('does NOT show the character if it is NOT selected', async () => {
		characterMock = { ...characterMock, selected: false };
		const { queryByText } = render(
			<Provider store={store}>
				<TeamSelectionAside />
			</Provider>
		);
		const character = queryByText('Luke Skywalker');
		expect(character).not.toBeInTheDocument();
	});
});
