import { useRouter } from 'next/router';
import { Box } from '@mui/material';
import { PATHS } from '@/00_global/constants';
import { getNextPreviousCharacter } from '@/00_global/utils/getNextPreviousCharacter';
import NextLink from '@/02_atoms/Link';
import React from 'react';
import useCharacters from '@/00_global/hooks/useCharacters';

const NextPreviousCharacter = () => {
	const router = useRouter();
	const { characters } = useCharacters();
	const { nextId, previousId } = getNextPreviousCharacter(characters, router.query.id);

	return (
		<Box display="flex" sx={{ justifyContent: 'space-between' }}>
			{previousId !== undefined && (
				<NextLink href={`${PATHS.CHARACTERS}/${previousId}`} variant={'outlined'}>
					Previous character
				</NextLink>
			)}
			{nextId !== undefined && (
				<NextLink href={`${PATHS.CHARACTERS}/${nextId}`} variant={'outlined'}>
					Next character
				</NextLink>
			)}
		</Box>
	);
};

export default NextPreviousCharacter;
