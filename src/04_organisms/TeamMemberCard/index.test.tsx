import '@testing-library/jest-dom';
import { fireEvent, render } from '@testing-library/react';
import { Provider } from 'react-redux';
import store from '@/00_global/store';
import { mockedCharacter } from '@/00_global/__mocks__';
import TeamMemberCard from '@/04_organisms/TeamMemberCard/index';

const mockRemoveFromMyTeam = jest.fn();

jest.mock('../../00_global/hooks/useCharacters', () => {
	return jest.fn(() => ({
		removeFromMyTeam: mockRemoveFromMyTeam
	}));
});

describe('TeamMemberCard', () => {
	afterEach(() => {
		jest.clearAllMocks();
	});

	it('renders a card with character information', () => {
		const { getByText, container } = render(
			<Provider store={store}>
				<TeamMemberCard character={mockedCharacter} />
			</Provider>
		);
		expect(container.querySelector('img')!.src).toBe(mockedCharacter.image);
		expect(getByText(mockedCharacter.name!)).toBeInTheDocument();
	});
	it('calls the handler to remove the character from my team', () => {
		const { getByText, container } = render(
			<Provider store={store}>
				<TeamMemberCard character={mockedCharacter} />
			</Provider>
		);
		const button = getByText('Remove');
		expect(button).toBeInTheDocument();
		fireEvent.click(button);
		expect(mockRemoveFromMyTeam).toHaveBeenCalledTimes(1);
	});
});
