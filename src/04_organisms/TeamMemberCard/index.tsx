import Box from '@mui/material/Box';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Typography from '@mui/material/Typography';
import { Character } from '@/00_global/types';
import { green } from '@mui/material/colors';
import { Button } from '@mui/material';
import React from 'react';
import useCharacters from '@/00_global/hooks/useCharacters';

const TeamMemberCard = ({ character }: { character: Character }) => {
	const { removeFromMyTeam } = useCharacters();

	return (
		<Card sx={{ display: 'flex' }}>
			<Box padding={2} sx={{ display: 'flex', flexDirection: 'row', alignItems: 'center', width: '100%' }}>
				<CardMedia
					component="img"
					sx={{ width: 50, backgroundColor: green[200], padding: '10px' }}
					image={character.image}
					alt={character.name}
					loading="lazy"
				/>
				<CardContent sx={{ flex: '1 0 auto' }}>
					<Typography
						component="h3"
						variant="h6"
						sx={{ whiteSpace: 'nowrap', overflow: 'hidden', textOverflow: 'ellipsis', maxWidth: '95%' }}
					>
						{character.name}
					</Typography>
					<Box sx={{ display: 'flex', alignItems: 'center' }}>
						<Button
							size="small"
							variant={'contained'}
							onClick={() => removeFromMyTeam(character.id)}
							disabled={!character.selected}
						>
							Remove
						</Button>
					</Box>
				</CardContent>
			</Box>
		</Card>
	);
};

export default TeamMemberCard;
