import '@testing-library/jest-dom';
import { fireEvent, render } from '@testing-library/react';
import { Provider } from 'react-redux';
import store from '@/00_global/store';
import CharacterCard from '@/04_organisms/CharacterCard/index';
import { mockedCharacter } from '@/00_global/__mocks__';

const mockAddToMyTeam = jest.fn();
const mockRemoveFromMyTeam = jest.fn();

jest.mock('../../00_global/hooks/useCharacters', () => {
	return jest.fn(() => ({
		addToMyTeam: mockAddToMyTeam,
		removeFromMyTeam: mockRemoveFromMyTeam
	}));
});

describe('CharacterCard', () => {
	afterEach(() => {
		jest.clearAllMocks();
	});

	it('renders a card with character information', () => {
		const { getByText, container } = render(
			<Provider store={store}>
				<CharacterCard character={mockedCharacter} myTeamLimitReached={false} />
			</Provider>
		);
		expect(container.querySelector('img')!.src).toBe(mockedCharacter.image);
		expect(getByText(mockedCharacter.name!)).toBeInTheDocument();
	});
	it('renders a card with selected character information', () => {
		const { getByText, container } = render(
			<Provider store={store}>
				<CharacterCard character={mockedCharacter} myTeamLimitReached={false} />
			</Provider>
		);
		const button = getByText('Remove from my team');
		expect(getByText('On my team!')).toBeInTheDocument();
		expect(button).toBeInTheDocument();
		fireEvent.click(button);
		expect(mockRemoveFromMyTeam).toHaveBeenCalledTimes(1);
	});
	it('renders a card with NOT selected character information', () => {
		const { getByText, container } = render(
			<Provider store={store}>
				<CharacterCard character={{ ...mockedCharacter, selected: false }} myTeamLimitReached={false} />
			</Provider>
		);
		const button = getByText('Add to my team');
		expect(getByText('Not on my team!')).toBeInTheDocument();
		expect(button).toBeInTheDocument();
		fireEvent.click(button);
		expect(mockAddToMyTeam).toHaveBeenCalledTimes(1);
	});
	it('does not allow a character to be added when myTeamLimitReached is true', () => {
		const { getByText, container } = render(
			<Provider store={store}>
				<CharacterCard character={{ ...mockedCharacter, selected: false }} myTeamLimitReached={true} />
			</Provider>
		);
		const button = getByText('Add to my team');
		expect(button).toBeInTheDocument();
		fireEvent.click(button);
		expect(mockAddToMyTeam).toHaveBeenCalledTimes(0);
	});
});
