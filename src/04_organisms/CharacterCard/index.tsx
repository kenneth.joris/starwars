import React from 'react';
import { Button, Card, CardActions, CardContent, CardMedia, Typography } from '@mui/material';
import { Character } from '@/00_global/types';
import { blue, green } from '@mui/material/colors';
import { PATHS } from '@/00_global/constants';
import NextLink from '@/02_atoms/Link';
import useCharacters from '@/00_global/hooks/useCharacters';

type CharacterProps = {
	character: Character;
	myTeamLimitReached: boolean;
};

const CharacterCard = ({ character, myTeamLimitReached }: CharacterProps) => {
	const { addToMyTeam, removeFromMyTeam } = useCharacters();

	return (
		<Card sx={{ minWidth: 250 }}>
			<CardMedia
				component="img"
				alt={character?.name}
				image={character?.image}
				sx={{ aspectRatio: '1/1', objectFit: 'contain', backgroundColor: character.selected ? green[200] : blue[50] }}
				loading="lazy"
			/>
			<CardContent>
				<Typography gutterBottom variant="h6" component="h2">
					{character?.name}
				</Typography>
				<Typography color="text.secondary" display={'flex'}>
					{character.selected ? 'On my team!' : 'Not on my team!'}
				</Typography>
			</CardContent>
			<CardActions>
				<NextLink href={`${PATHS.CHARACTERS}/${character.id}`}>Details</NextLink>
				{!character.selected && (
					<Button
						sx={{ marginLeft: 'auto' }}
						size="small"
						onClick={() => addToMyTeam(character.id)}
						disabled={myTeamLimitReached}
					>
						Add to my team
					</Button>
				)}
				{character.selected && (
					<Button sx={{ marginLeft: 'auto' }} size="small" onClick={() => removeFromMyTeam(character.id)}>
						Remove from my team
					</Button>
				)}
			</CardActions>
		</Card>
	);
};

export default CharacterCard;
