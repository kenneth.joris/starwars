import { AppBar, Box, Toolbar, Typography } from '@mui/material';
import { PATHS } from '@/00_global/constants';
import { usePathname } from 'next/navigation';
import NextLink from '@/02_atoms/Link';

const Navigation = () => {
	const pathname = usePathname();

	return (
		<AppBar
			position="static"
			color="default"
			elevation={0}
			sx={{ borderBottom: (theme) => `1px solid ${theme.palette.divider}` }}
		>
			<Toolbar sx={{ flexWrap: 'wrap', justifyContent: 'space-between' }}>
				<NextLink href={PATHS.HOME} variant={'text'}>
					<Typography component={'span'}>STARWARS</Typography>
				</NextLink>
				<nav>
					<Box display={'inline'} component={'li'} sx={{ listStyleType: 'none', marginRight: '10px' }}>
						<NextLink
							href={PATHS.CHARACTERS}
							variant={pathname.includes(PATHS.CHARACTERS) ? 'contained' : 'outlined'}
						>
							Characters
						</NextLink>
					</Box>
					<Box display={'inline'} component={'li'} sx={{ listStyleType: 'none' }}>
						<NextLink href={PATHS.MY_TEAM} variant={pathname.includes(PATHS.MY_TEAM) ? 'contained' : 'outlined'}>
							My Team
						</NextLink>
					</Box>
				</nav>
			</Toolbar>
		</AppBar>
	);
};

export default Navigation;
