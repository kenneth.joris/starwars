import CharactersPage from '@/05_templates/CharactersPage';
import { wrapper } from '@/00_global/store';
import { getAllCharacters, starwarsApi } from '@/00_global/services/starwars';

const Characters = () => {
	return <CharactersPage />;
};

export default Characters;

export const getStaticProps = wrapper.getStaticProps((store) => async (context) => {
	store.dispatch(getAllCharacters.initiate());
	await Promise.all(store.dispatch(starwarsApi.util?.getRunningQueriesThunk()));

	return {
		props: {}
	};
});
