import CharacterPage from '@/05_templates/CharacterPage';
import { setupStore, wrapper } from '@/00_global/store';
import { getAllCharacters, getCharacterById, getRunningQueriesThunk } from '@/00_global/services/starwars';
import { Character as CharacterType } from '@/00_global/types';

const Character = () => {
	return <CharacterPage />;
};

export default Character;

export async function getStaticPaths() {
	const store = setupStore();
	const characters = await store.dispatch(getAllCharacters.initiate());

	return {
		paths: characters?.data?.map((character: CharacterType) => `/characters/${character.id}`),
		fallback: false
	};
}

export const getStaticProps = wrapper.getStaticProps((store) => async (context) => {
	const id = context.params?.id;
	if (typeof id === 'string' && !isNaN(Number(id))) {
		store.dispatch(getCharacterById.initiate(Number(id)));
	}
	await Promise.all(store.dispatch(getRunningQueriesThunk()));

	return {
		props: {}
	};
});
