import type { AppProps } from 'next/app';
import { Provider } from 'react-redux';
import store, { wrapper } from '@/00_global/store';
import { Roboto } from 'next/font/google';
import { CssBaseline } from '@mui/material';
import Navigation from '@/04_organisms/Navigation';

const roboto = Roboto({
	subsets: ['latin'],
	weight: ['300', '400', '500', '700'],
	display: 'swap'
});

const App = ({ Component, ...rest }: AppProps) => {
	const { props } = wrapper.useWrappedStore(rest);
	return (
		<>
			<CssBaseline />
			<div className={`${roboto.className}`}>
				<Navigation />
				<Provider store={store}>
					<Component {...props.pageProps} />
				</Provider>
			</div>
		</>
	);
};

export default App;
