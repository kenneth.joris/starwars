import { Button, CardMedia, Container, Grid, Paper, Typography } from '@mui/material';
import NextPreviousCharacter from '@/04_organisms/NextPreviousCharacter';
import CharacterMeta from '@/03_molecules/CharacterMeta';
import React from 'react';
import TeamSelectionAside from '@/04_organisms/TeamSelectionAside';
import SectionTitle from '@/02_atoms/SectionTitle';
import useCharacters from '@/00_global/hooks/useCharacters';

const CharacterPage = () => {
	const { character, myTeamLimitReached, addToMyTeam, removeFromMyTeam } = useCharacters();

	return (
		<Container maxWidth="xl" component="main" sx={{ pt: 8, pb: 8 }}>
			<NextPreviousCharacter />
			<Grid container spacing={2} sx={{ marginTop: '2rem' }}>
				{/* DETAILS */}
				<Grid item xs={12} md={8} lg={9}>
					<SectionTitle>Character Details</SectionTitle>
					<Grid container spacing={2} sx={{ marginTop: '2rem' }}>
						<Grid item xs={12} md={4}>
							<CardMedia
								component="img"
								sx={{ width: '100%' }}
								image={character?.image}
								alt={character?.name ?? ''}
								loading="lazy"
							/>
						</Grid>
						<Grid item xs={12} md={8} component={'ul'} sx={{ paddingLeft: '30px' }}>
							<Typography variant="h5" component={'h1'} sx={{ marginBottom: '1rem' }}>
								{character?.name}
							</Typography>
							<Typography variant="body2" component={'h2'} sx={{ marginBottom: '1rem' }}>
								{character?.name} is {character.selected ? '' : 'not'} a team-member.
							</Typography>
							<Button
								sx={{ marginLeft: 'auto' }}
								size="small"
								variant={'contained'}
								onClick={() => addToMyTeam(character.id)}
								disabled={character.selected || myTeamLimitReached}
							>
								Add to my team
							</Button>
							<Button
								sx={{ marginLeft: '1rem' }}
								size="small"
								variant={'contained'}
								onClick={() => removeFromMyTeam(character.id)}
								disabled={!character.selected}
							>
								Remove from my team
							</Button>
							{myTeamLimitReached && (
								<Typography variant="caption" component={'p'} color={'darkorange'} sx={{ marginTop: '1rem' }}>
									Your team is full!
								</Typography>
							)}
							<Paper elevation={0} sx={{ padding: '15px 30px' }} component={'ul'}>
								{Object.entries(character ?? {})
									.filter(([key]) => key !== 'name' && key !== 'image' && key !== 'selected')
									.map(([key, value]) => (
										<li key={key}>
											<Typography variant="button" display="inline">
												{key}:{' '}
											</Typography>
											<CharacterMeta data={value} />
										</li>
									))}
							</Paper>
						</Grid>
					</Grid>
				</Grid>

				{/* ASIDE */}
				<Grid item xs={12} md={4} lg={3} component={'aside'} sx={{ position: 'sticky', top: '0', alignSelf: 'start' }}>
					<TeamSelectionAside />
				</Grid>
			</Grid>
		</Container>
	);
};

export default CharacterPage;
