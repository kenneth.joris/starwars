import useCharacters from '@/00_global/hooks/useCharacters';
import { Container, Grid, Typography } from '@mui/material';
import CharacterCard from '@/04_organisms/CharacterCard';
import React from 'react';
import SectionTitle from '@/02_atoms/SectionTitle';

const MyTeamPage = () => {
	const { characters, myTeamLimitReached } = useCharacters();
	const selectedCharacters = characters?.filter((character) => character.selected);

	return (
		<Container maxWidth="xl" component="main" sx={{ pt: 8, pb: 8 }}>
			<SectionTitle>My Team</SectionTitle>
			<Grid container spacing={2}>
				{!selectedCharacters?.length && (
					<Grid item xs={12}>
						<Typography variant="body2" color="text.secondary" component="p" marginBottom={2}>
							Select some characters first!
						</Typography>
					</Grid>
				)}
				{selectedCharacters?.map((character) => (
					<Grid key={character.id} item xs={12} sm={6} md={4} lg={3}>
						<CharacterCard character={character} myTeamLimitReached={myTeamLimitReached} />
					</Grid>
				))}
			</Grid>
		</Container>
	);
};

export default MyTeamPage;
