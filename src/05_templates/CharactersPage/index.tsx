import { Container, Grid, Typography } from '@mui/material';
import CharacterCard from '@/04_organisms/CharacterCard';
import useCharacters from '@/00_global/hooks/useCharacters';
import React from 'react';
import TeamSelectionAside from '@/04_organisms/TeamSelectionAside';
import SectionTitle from '@/02_atoms/SectionTitle';

const CharactersPage = () => {
	const { characters, myTeamLimitReached } = useCharacters();

	return (
		<Container maxWidth="xl" component="main" sx={{ pt: 8, pb: 8 }}>
			<Grid container spacing={2}>
				<Grid item xs={12} md={8} lg={9} xl={10}>
					<SectionTitle>Characters</SectionTitle>
					<Grid container spacing={6}>
						{characters?.map((character) => (
							<Grid key={character.id} item xs>
								<CharacterCard character={character} myTeamLimitReached={myTeamLimitReached} />
							</Grid>
						))}
					</Grid>
				</Grid>
				<Grid
					item
					xs={12}
					md={4}
					lg={3}
					xl={2}
					component={'aside'}
					sx={{ position: 'sticky', top: '0', alignSelf: 'start' }}
				>
					<TeamSelectionAside />
				</Grid>
			</Grid>
		</Container>
	);
};

export default CharactersPage;
