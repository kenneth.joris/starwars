import { Container, Grid, Typography } from '@mui/material';
import { PATHS } from '@/00_global/constants';
import React from 'react';
import NextLink from '@/02_atoms/Link';

const HomePage = () => {
	return (
		<Container maxWidth="sm" component="main" sx={{ pt: 8, pb: 6 }}>
			<Typography component="h1" variant="h2" align="center" color="text.primary" gutterBottom>
				Use the Force!
			</Typography>
			<Typography variant="h5" align="center" color="text.secondary" component="p">
				You are the chosen one! It is said that you will destroy the Sith. You will to bring balance to the Force.
			</Typography>
			<Grid container direction="row" justifyContent="center" alignItems="center" sx={{ marginTop: '2rem' }}>
				<NextLink href={PATHS.CHARACTERS} variant={'outlined'}>
					Create my team
				</NextLink>
			</Grid>
		</Container>
	);
};

export default HomePage;
