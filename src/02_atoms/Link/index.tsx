import Link, { LinkProps } from 'next/link';
import { Button, ButtonPropsVariantOverrides } from '@mui/material';
import React from 'react';
import { OverridableStringUnion } from '@mui/types';

interface NextLinkProps {
	children: React.ReactNode;
	variant?: OverridableStringUnion<'text' | 'outlined' | 'contained', ButtonPropsVariantOverrides> | undefined;
}

const NextLink = ({ href, as, prefetch, variant, children, ...props }: LinkProps & NextLinkProps) => (
	<Link href={href} as={as} prefetch={prefetch} {...props}>
		<Button variant={variant} color="primary" size="small" component={'span'}>
			{children}
		</Button>
	</Link>
);

export default NextLink;
