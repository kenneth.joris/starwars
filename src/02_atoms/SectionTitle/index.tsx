import { Typography } from '@mui/material';
import React from 'react';

const SectionTitle = ({ children }: { children: React.ReactNode }) => {
	return (
		<Typography variant="h5" color="text.secondary" component="h2" marginBottom={2}>
			{children}
		</Typography>
	);
};

export default SectionTitle;
