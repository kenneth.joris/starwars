import { Character } from '@/00_global/types';

const darkSideRegex = /(darth|sith)/gi;

export const isDarkSide = (data: string | string[]) => {
	if (Array.isArray(data)) {
		return data.some((string) => !!string.match(darkSideRegex)?.length);
	}
	return !!data.match(darkSideRegex)?.length;
};

export const excludeDarkSideCharacters = (characters: Character[] = []): Character[] => {
	return characters.filter((character) => {
		const name = character?.name ?? '';
		const affiliations = character?.affiliations ?? [];
		const masters = character?.masters ?? [];
		return !isDarkSide(name) && !isDarkSide(affiliations) && !isDarkSide(masters);
	});
};
