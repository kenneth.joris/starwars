import { Character } from '@/00_global/types';

type TransformCharacterProps = {
	stateCharacters: Character[];
	payloadCharacter: Character;
};

type TransformCharactersProps = {
	stateCharacters: Character[];
	payloadCharacters: Character[];
};

export const transformCharacter = ({ stateCharacters, payloadCharacter }: TransformCharacterProps) => {
	const payloadCharacterId = payloadCharacter.id;
	const stateCharacter = stateCharacters.find((stateCharacter) => payloadCharacterId === stateCharacter.id);
	return {
		...payloadCharacter,
		selected: stateCharacter?.selected ?? false
	};
};

export const transformCharacters = ({ stateCharacters, payloadCharacters }: TransformCharactersProps) => {
	return payloadCharacters.map((payloadCharacter) =>
		transformCharacter({
			stateCharacters: stateCharacters,
			payloadCharacter: payloadCharacter
		})
	);
};
