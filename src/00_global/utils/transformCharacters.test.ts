import { transformCharacter, transformCharacters } from '@/00_global/utils/transformCharacters';
import { Character } from '@/00_global/types';

describe('transformCharacter', () => {
	it('stateCharacters selected state is preserved when a payloadCharacter is received', () => {
		const stateCharacters = [{ id: 1, selected: true } as Character];
		const payloadCharacter = { id: 1 } as Character;
		const result = transformCharacter({ stateCharacters, payloadCharacter });
		expect(result).toStrictEqual({ id: 1, selected: true });
	});
	it('stateCharacters selected state is false when selected state is not set, and a payloadCharacter is received', () => {
		const stateCharacters = [{ id: 1 } as Character];
		const payloadCharacter = { id: 1 } as Character;
		const result = transformCharacter({ stateCharacters, payloadCharacter });
		expect(result).toStrictEqual({ id: 1, selected: false });
	});
	it('selected state is false when no matching stateCharacters is found, and a payloadCharacter is received', () => {
		const stateCharacters = [{ id: 0 } as Character];
		const payloadCharacter = { id: 1 } as Character;
		const result = transformCharacter({ stateCharacters, payloadCharacter });
		expect(result).toStrictEqual({ id: 1, selected: false });
	});
});

describe('transformCharacters', () => {
	it('stateCharacters selected state is preserved when payloadCharacters are received', () => {
		const stateCharacters = [{ id: 1, selected: true } as Character];
		const payloadCharacters = [{ id: 1 } as Character];
		const result = transformCharacters({ stateCharacters, payloadCharacters });
		expect(result).toStrictEqual([{ id: 1, selected: true }]);
	});
	it('stateCharacters selected state is false when selected state is not set, and payloadCharacters are received', () => {
		const stateCharacters = [{ id: 1 } as Character];
		const payloadCharacters = [{ id: 1 } as Character];
		const result = transformCharacters({ stateCharacters, payloadCharacters });
		expect(result).toStrictEqual([{ id: 1, selected: false }]);
	});
	it('selected state is false when no matching stateCharacters is found, and payloadCharacters are received', () => {
		const stateCharacters = [{ id: 0 } as Character];
		const payloadCharacters = [{ id: 1 } as Character];
		const result = transformCharacters({ stateCharacters, payloadCharacters });
		expect(result).toStrictEqual([{ id: 1, selected: false }]);
	});
});
