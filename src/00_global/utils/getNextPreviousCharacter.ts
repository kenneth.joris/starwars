import { Character } from '@/00_global/types';

export const getNextPreviousCharacter = (characters: Character[] | undefined, pageId: string | string[] | undefined) => {
	const currentCharacterIndex = characters?.findIndex((character) => {
		const characterId = character.id + '';
		return characterId === pageId;
	});

	if (!characters?.length || currentCharacterIndex === undefined || currentCharacterIndex === -1) {
		return {
			previousId: undefined,
			nextId: undefined
		};
	}

	const previousId = characters?.[currentCharacterIndex === 0 ? characters.length - 1 : currentCharacterIndex - 1];
	const nextId = characters?.[currentCharacterIndex === characters.length - 1 ? 0 : currentCharacterIndex + 1];

	return {
		previousId: previousId.id,
		nextId: nextId.id
	};
};
