import { excludeDarkSideCharacters, isDarkSide } from './excludeDarkSideCharacters';
import { expect } from '@jest/globals';
import { Character } from '@/00_global/types';
describe('isDarkSide', () => {
	it('returns true if string matches the darkSideRegex darth', () => {
		const result = isDarkSide('a darth character');
		expect(result).toBeTruthy();
	});
	it('returns true if string[] matches the darkSideRegex darth', () => {
		const result = isDarkSide(['a darth character', 'xxx']);
		expect(result).toBeTruthy();
	});
	it('returns true if string matches the darkSideRegex sith', () => {
		const result = isDarkSide('a sith character');
		expect(result).toBeTruthy();
	});
	it('returns true if string[] matches the darkSideRegex sith', () => {
		const result = isDarkSide(['a sith character', 'xxx']);
		expect(result).toBeTruthy();
	});
	it('returns false if string does not match the darkSideRegex', () => {
		const result = isDarkSide('Some string');
		expect(result).toBeFalsy();
	});
	it('returns false if string[] does not match the darkSideRegex', () => {
		const result = isDarkSide(['Some string', 'Some string']);
		expect(result).toBeFalsy();
	});
});

describe('excludeDarkSideCharacters', () => {
	it('returns true if a characters name matches the darkSideRegex darth', () => {
		const result = excludeDarkSideCharacters([
			{
				name: 'darth',
				affiliations: ['xxx'],
				masters: ['xxx']
			} as Character
		]);
		expect(result).toBeTruthy();
	});
	it('returns true if a characters affiliations matches the darkSideRegex darth', () => {
		const result = excludeDarkSideCharacters([
			{
				name: 'xxx',
				affiliations: ['darth', 'xxx'],
				masters: ['xxx']
			} as Character
		]);
		expect(result).toBeTruthy();
	});
	it('returns true if a characters masters matches the darkSideRegex darth', () => {
		const result = excludeDarkSideCharacters([
			{
				name: 'xxx',
				affiliations: ['xxx'],
				masters: ['darth', 'xxx']
			} as Character
		]);
		expect(result).toBeTruthy();
	});
	it('returns true if a characters name matches the darkSideRegex sith', () => {
		const result = excludeDarkSideCharacters([
			{
				name: 'sith',
				affiliations: ['xxx'],
				masters: ['xxx']
			} as Character
		]);
		expect(result).toBeTruthy();
	});
	it('returns true if a characters affiliations matches the darkSideRegex sith', () => {
		const result = excludeDarkSideCharacters([
			{
				name: 'xxx',
				affiliations: ['sith', 'xxx'],
				masters: ['xxx']
			} as Character
		]);
		expect(result).toBeTruthy();
	});
	it('returns true if a characters masters matches the darkSideRegex sith', () => {
		const result = excludeDarkSideCharacters([
			{
				name: 'xxx',
				affiliations: ['xxx'],
				masters: ['sith', 'xxx']
			} as Character
		]);
		expect(result).toBeTruthy();
	});
});
