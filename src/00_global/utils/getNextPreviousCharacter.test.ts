import { getNextPreviousCharacter } from '@/00_global/utils/getNextPreviousCharacter';
import { Character } from '@/00_global/types';

const testCharacters = [{ id: 1 } as Character, { id: 2 } as Character, { id: 3 } as Character];

describe('getNextPreviousCharacter', () => {
	it('returns the prev and next character url as 3 and 2 if the current pageId is "1"', () => {
		const result = getNextPreviousCharacter(testCharacters, '1');
		expect(result).toStrictEqual({
			previousId: 3,
			nextId: 2
		});
	});
	it('returns the prev and next character url as 1 and 3 if the current pageId is "2"', () => {
		const result = getNextPreviousCharacter(testCharacters, '2');
		expect(result).toStrictEqual({
			previousId: 1,
			nextId: 3
		});
	});
	it('returns the prev and next character url as 2 and 1 if the current pageId is "3"', () => {
		const result = getNextPreviousCharacter(testCharacters, '3');
		expect(result).toStrictEqual({
			previousId: 2,
			nextId: 1
		});
	});
	it('returns undefined for prev and next if no chararacters provided', () => {
		const result = getNextPreviousCharacter([], '1');
		expect(result).toStrictEqual({
			previousId: undefined,
			nextId: undefined
		});
	});
	it('returns undefined for prev and next if no pageId provided', () => {
		const result = getNextPreviousCharacter(testCharacters, '');
		expect(result).toStrictEqual({
			previousId: undefined,
			nextId: undefined
		});
	});
	it('returns undefined for prev and next if the pageId does not match any characters', () => {
		const result = getNextPreviousCharacter(testCharacters, '100');
		expect(result).toStrictEqual({
			previousId: undefined,
			nextId: undefined
		});
	});
});
