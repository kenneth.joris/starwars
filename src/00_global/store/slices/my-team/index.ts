import { createAction, createSlice, PayloadAction } from '@reduxjs/toolkit';
import { Character } from '@/00_global/types';
import { starwarsApi } from '@/00_global/services/starwars';
import { transformCharacter, transformCharacters } from '@/00_global/utils/transformCharacters';
import { HYDRATE } from 'next-redux-wrapper';
import { RootState } from '@/00_global/store';

export type InitialState = {
	character: Character;
	characters: Character[];
	myTeamLimitReached: boolean;
};

// SSG Hydration
const APP_HYDRATE = createAction<RootState>(HYDRATE);

export const myTeamSlice = createSlice({
	name: 'myTeam',
	initialState: <InitialState>{
		character: {} as Character,
		characters: [],
		myTeamLimitReached: false
	},
	reducers: {
		addToMyTeam: (state, action: PayloadAction<number>) => {
			if (state.myTeamLimitReached) return;

			const characterId = action.payload;

			// update characters
			state.characters.map((character) => {
				if (character.id === characterId) {
					character.selected = true;
				}
				return character;
			});

			// update character
			const stateCharacterId = state.character?.id;
			if (!!stateCharacterId && stateCharacterId === characterId) {
				state.character.selected = true;
			}

			// keep track of the team limit
			const charactersInMyTeam = state.characters.filter((character) => character.selected);
			state.myTeamLimitReached = charactersInMyTeam.length >= 5;
		},
		removeFromMyTeam: (state, action: PayloadAction<number>) => {
			const characterId = action.payload;

			// update characters
			state.characters.map((character) => {
				if (character.id === characterId) {
					character.selected = false;
				}
				return character;
			});

			// update character
			const stateCharacterId = state.character?.id;
			if (!!stateCharacterId && stateCharacterId === characterId) {
				state.character.selected = false;
			}

			// keep track of the team limit
			state.myTeamLimitReached = false;
		}
	},
	extraReducers: (builder) => {
		builder
			// SSG
			.addCase(APP_HYDRATE, (state, action) => {
				const clientState = state;
				const serverState = action.payload.myTeam;

				// State reconciliation during hydration
				return {
					character: { ...serverState.character, ...clientState.character },
					characters: !!clientState.characters.length ? clientState.characters : serverState.characters,
					myTeamLimitReached: clientState.myTeamLimitReached
				};
			})
			// APIs
			.addMatcher(starwarsApi.endpoints.getAllCharacters.matchFulfilled, (state, { payload }) => {
				state.characters = transformCharacters({
					stateCharacters: state.characters,
					payloadCharacters: payload
				});
			})
			.addMatcher(starwarsApi.endpoints.getCharacterById.matchFulfilled, (state, { payload }) => {
				state.character = transformCharacter({
					stateCharacters: state.characters,
					payloadCharacter: payload
				});
			});
	}
});

export const { addToMyTeam, removeFromMyTeam } = myTeamSlice.actions;
export default myTeamSlice.reducer;
