import { combineReducers, configureStore } from '@reduxjs/toolkit';
import myTeamReducer from './slices/my-team';
import { starwarsApi } from '@/00_global/services/starwars';
import { createWrapper } from 'next-redux-wrapper';
import { setupListeners } from '@reduxjs/toolkit/query';

const rootReducer = combineReducers({
	myTeam: myTeamReducer,
	[starwarsApi.reducerPath]: starwarsApi.reducer
});

export const setupStore = () =>
	configureStore({
		reducer: rootReducer,
		middleware: (getDefaultMiddleware) => getDefaultMiddleware().concat(starwarsApi.middleware)
	});

const store = setupStore();
setupListeners(store.dispatch);
export default store;

export type AppStore = ReturnType<typeof setupStore>;
export type RootState = ReturnType<typeof rootReducer>;
export type AppDispatch = typeof store.dispatch;

// SSG Logic
export const wrapper = createWrapper<AppStore>(setupStore, { debug: false });
