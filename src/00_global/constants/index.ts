export const API_ENDPOINT = {
	STARWARS: 'https://rawcdn.githack.com/akabab/starwars-api/0.2.1/api'
} as const;

export const PATHS = {
	HOME: '/',
	CHARACTERS: '/characters',
	MY_TEAM: '/my-team'
} as const;
