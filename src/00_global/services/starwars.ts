import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react';
import { API_ENDPOINT } from '@/00_global/constants';
import { Character } from '@/00_global/types';
import { HYDRATE } from 'next-redux-wrapper';
import { excludeDarkSideCharacters } from '@/00_global/utils/excludeDarkSideCharacters';

export const starwarsApi = createApi({
	reducerPath: 'starwarsApi',
	baseQuery: fetchBaseQuery({ baseUrl: API_ENDPOINT.STARWARS }),
	tagTypes: ['Characters', 'Character'],
	endpoints: (builder) => ({
		getAllCharacters: builder.query<Character[], void>({
			query: () => ({ url: `/all.json` }),
			transformResponse: (responseData) => {
				return excludeDarkSideCharacters(responseData as Character[]);
			},
			providesTags: ['Characters']
		}),
		getCharacterById: builder.query<Character, number>({
			query: (id: number) => ({ url: `/id/${id}.json` }),
			providesTags: ['Character']
		})
	}),
	// SSG
	extractRehydrationInfo(action, { reducerPath }) {
		if (action.type === HYDRATE) {
			return action.payload[reducerPath];
		}
	}
});

export const {
	useGetAllCharactersQuery,
	useGetCharacterByIdQuery,
	// for use in SSG
	util: { getRunningQueriesThunk }
} = starwarsApi;

// exports for use in SSG
export const { getAllCharacters, getCharacterById } = starwarsApi.endpoints;
