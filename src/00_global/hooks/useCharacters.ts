import { useGetAllCharactersQuery, useGetCharacterByIdQuery } from '@/00_global/services/starwars';
import { useDispatch, useSelector } from 'react-redux';
import { RootState } from '@/00_global/store';
import { useRouter } from 'next/router';
import { useEffect } from 'react';
import { addToMyTeam, removeFromMyTeam } from '@/00_global/store/slices/my-team';

const useCharacters = () => {
	useGetAllCharactersQuery();
	const router = useRouter();
	const dispatch = useDispatch();
	const state = useSelector((state: RootState) => state.myTeam);
	const { refetch, isUninitialized } = useGetCharacterByIdQuery(Number(router.query.id), {
		skip: !router.query.id
	});

	useEffect(() => {
		!isUninitialized && router.query.id &&  refetch();
	}, [isUninitialized, router.query.id, refetch]);

	return {
		character: state.character,
		characters: state.characters,
		myTeamLimitReached: state.myTeamLimitReached,
		addToMyTeam: (id: number) => dispatch(addToMyTeam(id)),
		removeFromMyTeam: (id: number) => dispatch(removeFromMyTeam(id))
	};
};

export default useCharacters;
