import { render } from '@testing-library/react';
import CharacterMeta from '@/03_molecules/CharacterMeta/index';

describe('CharacterMeta', () => {
	it('shows Unknown if an empty array is given', () => {
		const { getByText } = render(<CharacterMeta data={[]} />);
		expect(getByText('Unknown')).toBeTruthy();
	});
	it('shows the boolean value if a boolean is given', () => {
		const { getByText } = render(<CharacterMeta data={true} />);
		expect(getByText('true')).toBeTruthy();
	});
	it('shows the string if a string is given', () => {
		const { getByText } = render(<CharacterMeta data={'string'} />);
		expect(getByText('string')).toBeTruthy();
	});
	it('shows the number if a number is given', () => {
		const { getByText } = render(<CharacterMeta data={1} />);
		expect(getByText('1')).toBeTruthy();
	});
	it('shows comma separated strings if a string[] is given', () => {
		const { container } = render(<CharacterMeta data={['string', 'string']} />);
		expect(container.textContent).toBe('string, string');
	});
});
