import { Typography } from '@mui/material';

const CharacterMeta = ({ data }: { data: boolean | string | number | string[] }) => {
	if (Array.isArray(data)) {
		if (!data.length) {
			return (
				<Typography variant="caption" display={'inline'}>
					Unknown
				</Typography>
			);
		}
		return data.map((value: string | number, index) => (
			<Typography variant="caption" key={index}>
				{index !== 0 && ', '}
				{value}
			</Typography>
		));
	}
	return (
		<Typography variant="caption" display={'inline'}>
			{data.toString()}
		</Typography>
	);
};

export default CharacterMeta;
