const nextJest = require('next/jest');
const createJestConfig = nextJest({
	dir: './'
});
const customJestConfig = {
	verbose: true,
	moduleDirectories: ['node_modules', '<rootDir>/'],
	testEnvironment: 'jest-environment-jsdom',
	setupFiles: ['<rootDir>/jest.polyfill.js'],
	testEnvironmentOptions: {
		customExportConditions: ['']
	}
};
module.exports = createJestConfig(customJestConfig);
