# Starwars Front-end Coding Test
**Intro**

In this technical front-end developer test, you're going to build a simple application. The objective is to test your skills, to measure how you solve coding challenges and do research.

We’re open to feedback regarding this exercise.

Your code will be evaluated according to following criteria:

- TypeScript usage
- React usage (design patterns etc.)
- State management (Redux + Redux Toolkit)
- Coding style (we like clean code)
- Solution & architecture design

**Prerequisites** 

You need a development environment for a TypeScript application with a build tool of your choice. If you like, you can use [create-next-app](https://www.npmjs.com/package/create-next-app) to get started.

**Requirements**

We’re assembling a team to fight the dark side! Our application will assist us in creating a team of maximum 5 Star Wars characters. Our team is determined to eradicate evil, which means **no evil members are allowed to join**. 

- Create a Next.js application that shows a list of Star Wars characters
- The application should have a detail page for every character
- Each detail page shows some basic information about the selected character (name, image, height, mass, affiliations)
- Each detail page has a next and previous button to navigate between characters
- Each detail page allows us to add and/or remove the selected character from our team
- The application should have a team page showing your assembled team. We should be able to remove characters from our team from this page as well.
- We should be able to see and manage our assembled team from every page
- A team has max. 5 members

A character is defined as evil when:

- They have ‘Darth’ or ‘Sith’ in their name
- They have at least one affiliation that mentions ‘Darth’ or ‘Sith’ 
  (you may ignore former affiliations)
- They have at least one master with ‘Darth’ in their name

APIs provided:

<https://akabab.github.io/starwars-api/>

**Some ideas to earn bonus points**

1. Pagination for main list of characters
1. Create (unit) tests for your components and/or business logic
1. Usage of MaterialUI theming
1. Add prettier and/or linting
1. Use your creativity to add a personal touch

**Final steps**

1. Add some documentation in a README file

Good luck, and may the force be with you! 

## DEVELOPMENT

We’re assembling a team to fight the dark side! Our application will assist us in creating a team of maximum 5 Star Wars characters. Our team is determined to eradicate evil, which means no evil members are allowed to join.

First, install:

```bash
npm install
```
Second, run the development server:

```bash
npm run dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

## Learn More

### Architecture

In this project we make use of:
- Atomic Design Methodologie [More info](https://atomicdesign.bradfrost.com/)
- Next.js (Pages-router)
- Redux Toolkit and RTK Query
- Jest and Testing-library
- Prettier and EsLint
- Material UI
- Typescript.